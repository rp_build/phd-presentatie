% Hydro-metallurgic separation and synthesis in mesoflow reactors.
% Tobias Vandermeersch
% 20 januari 2016

# Vraagstelling
w

#  van het onderzoek

# Druppel generatie multifase
$ l/w = 1 + \alpha Q_c Q_d^2$ channel lengt and droplet width
$\mathrm{Ca} = \frac{\mu V}{\gamma} $ Capillary number:  viscous forces versus surface tension

# Energie dissipatie en mass transfer

# Multi stage solvent extractie

# Warmte en energie transfer in flow reactoren

# Kristal synthese in flow reactoren

# Toepassingsgebieden van flow reactoren

# Futures 

#tips van jonas
Ik ga u de presentatie doorsturen via wetransfer (beetje te groot). 

Tips:
* Volgens de richtlijnen moet je presentatie tussen de 15min en 30 min duren. Maar je maakt ze best zo dat ze ongeveer 20min duurt (= standaard)
* Als je kunt maak je best een slide met je research questions. Dan is het heel duidelijk wat de doelen waren van je doctoraat. Wim heeft dat graag en in mijn jury ook een jurylid (Rob Lammertink) dat zo iets graag zag (volgens Wim).
* Bij mij was de volledige jury Nederlandstalig, dus was mijn verdediging ook in het Nederlands. Ik had het echter in het Engels voorbereid en dan ter plekke het plots in het Nederlands moeten doen heeft ervoor gezorgd dat ik veel "Euh's" heb gebruikt. Dus probeer te achterhalen wat de voertaal gaat zijn. (pols eens bij Wim. Meestal kan voorzitter je hier ook bij helpen)
* Je gaat nooit al je resultaten kunnen bespreken, dus zorg ervoor dat je wat selecteert en het een mooi verhaal is en laat vooral de resultaten zien waar je het meest trots op bent.
* Ga op voorhand ook al eens dat lokaal bekijken en bv. de tafels zo wat te schuiven dat het eenvoudigst is voor u. Normaal zit de Jury aan 1 grote tafel, waar jij dan ook plaats neemt. Enkel tijdens de presentatie sta je recht.
* Ik ben tegen veel tekst op slides en gebruik bijgevolg veel figuren
* Een inhoudstafel op elke slide (onderaan of langs zijkant) vind ik ook heel handig voor de jury, zodat ze goed kunnen volgen
* Nummer ook zeker je slides zodat juryleden ernaar kunnen verwijzen
* Je kan proberen te anticiperen op vragen en al wat slides hiervoor voorbereiden. Ik had ook voor elke figuur die in mijn doc stond een hyperlink gemaakt in de presentatie naar die figuur, zodat indien nodig ik die gemakkelijk kan oproepen. Uiteindelijk wel niet gebruikt :).
* Misschien belangrijkste: proberen rustig te blijven en je gaat ongetwijfeld wel is keertje hebben dat je niet op een vraag kan antwoorden. Niet te hard mee inzitten :), ik had dat ook hoor.