% Hydro metallurgic separation and synthesis in meso flow reactors.
% Tobias Vandermeersch
% 20 januari 2016

#Zeolieten

## NaA
[website](http://izasc.biw.kuleuven.be/fmi/xsl/IZA-SC/ftc_fw.xsl?-db=Atlas_main&-lay=fw&-max=25&STC=LTA&-find)

![LTA](./img/zeolite/LTA.jpg){width=10%}

## NaX
[website](http://izasc.biw.kuleuven.be/fmi/xsl/IZA-SC/ftc_fw.xsl?-db=Atlas_main&-lay=fw&-max=25&STC=FAU&-find)

![FAU](./img/zeolite/NaX.jpg){width=10%}

## Sodalite
[website](http://izasc.biw.kuleuven.be/fmi/xsl/IZA-SC/ftc_fw.xsl?-db=Atlas_main&-lay=fw&-max=25&STC=SOD&-find)

![SOD](./img/zeolite/SOD.gif){width=10%}

# Polymeren
* [PMMA](https://en.wikipedia.org/wiki/Poly\(methyl_methacrylate\))
* [PC](https://en.wikipedia.org/wiki/Polycarbonate)
* [PDMS](https://en.wikipedia.org/wiki/Polydimethylsiloxane)
* [COC/COP](https://en.wikipedia.org/wiki/Cyclic_olefin_copolymer)

# Solventen en chemicalien
* [DCM](https://en.wikipedia.org/wiki/Dichloromethane)
* [IPA](https://en.wikipedia.org/wiki/Isopropyl_alcohol)
* [FITC](https://en.wikipedia.org/wiki/Fluorescein)
  - excitatie 494 nm
  - emissie 521 nm
  - pKa = 6.4 (emissie/excitatie pH 5-9)

* [Succinic acid](https://en.wikipedia.org/wiki/Succinic_acid)
  - 1,4-butanediol
  - pKa = 4.2
  - pKa = 5.6 

# Technieken
* [XRD](https://en.wikipedia.org/wiki/X-ray_crystallography)
* [SEM](https://en.wikipedia.org/wiki/Scanning_electron_microscope)
* [LS](http://www.shimadzu.com/an/powder/support/middle/m01.html)
    - brownian motion?
    - braggs diffraction
* [licht spectrum](https://en.wikipedia.org/wiki/Electromagnetic_spectrum)

---

# Chapter 0

## What is process intensification {.fragile}
[article](http://www.sciencedirect.com/science/article/pii/S0255270111002923) 
[definitie](http://biotech.about.com/od/glossary/g/ProcessIntense.htm)

>Process Intensification (PI) is an engineering expression that refers to making changes that render a manufacturing or processing design substantially improved in terms of energy efficiency, cost-effectiveness or enhancement of other qualities. Through PI, biotech companies strive to enhance production of biological products (i.e. in fermentation units or other bioreactors), by decreasing energy consumption, increasing reaction rates, reducing wasted energy and costs associated with waste products, improving purification steps, reducing equipment size, increasing safety and operational simplicity, etc. In doing so, companies can increase the sustainability of their company activities. In doing so, companies can increase the sustainability of their company activities.

 

## Waar past hydrometallurgie {.fragile}
Hydrometallurgy is a method for obtaining metals from their ores. It is a technique within the field of extractive metallurgy involving the use of aqueous chemistry for the recovery of metals from ores, concentrates, and recycled or residual materials. 1 and 2 Metal chemical processing techniques that complement hydrometallurgy are pyrometallurgy, vapour metallurgy and molten salt electrometallurgy. Hydrometallurgy is typically divided into three general areas:

 
* Leaching
* Solution concentration and purification
* Metal or metal compound recovery

--- 

# Chapter 1 {.fragile}


* massa transfer
* warmte transfer
* drijvende kracht
* snelheid
* hoe wordt chemische optimalisatie gezien
* wat is laminair en turbulent
* wat is chaotische advectie
* wat is moleculaire diffusie
* wat is turbulente menging
* [taylor dispersie](https://en.wikipedia.org/wiki/Taylor_dispersion)
* [radiale en axiale dispersie](./data/axial_dispersion.pdf)
* wat is typische oppervlakte voor warmte overdracht in een batch reactor
* [wat is microwave heating hoe werkt het, wat zijn voor en nadelen op welk](https://en.wikipedia.org/wiki/Dielectric_heating)
* [wat is ultrasoon en hoe werkt het](https://nl.wikipedia.org/wiki/Ultrageluid)
* [wat is de kolmogorov scale](https://en.wikipedia.org/wiki/Kolmogorov_microscales)
* wat is eddy diffusion
* wat zijn typische peclet en reynolds nummers dat wij halen?
* [rayleigh taylor](https://www.thermalfluidscentral.org/encyclopedia/index.php/Rayleigh-Taylor_Instability)
* [2 film theorie uitleggen met voorbeeld (levenspiel)](./data/two_film_theory.pdf)
* [wat is complexatie?](https://en.wikipedia.org/wiki/Coordination_complex)
* [wat is activity coefficient](https://en.wikipedia.org/wiki/Activity_coefficient)
* henry en raoult law
* [chemical potential](https://en.wikipedia.org/wiki/Chemical_potential)
* sauce:
  - https://www.youtube.com/watch?v=kXio3EcxX3M
  - https://en.wikipedia.org/wiki/Thermodynamic_potential
* [temperature dependency of diffusion rate](https://en.wikipedia.org/wiki/Diffusion)
* [wat is oppervlakte spanning](https://en.wikipedia.org/wiki/Surface_tension)
* wat is een grace plot
* [shear forces](https://en.wikipedia.org/wiki/Fluid_mechanics)
* [wat is eddy vortices and dissipation](https://en.wikipedia.org/wiki/Eddy_\(fluid_dynamics\))
* theory van kolmogorov, hinze en davies uitleggen
* [correlatie impeller speed and shape and energy dissipation](./data/impeller_droplet_size.pdf)
* [gibbs free energy](https://en.wikipedia.org/wiki/Gibbs_free_energy)
* [oswald ripening](https://en.wikipedia.org/wiki/Ostwald_ripening)

---

# Chapter 2 {.fragile}

 
* voorbeelden voor primary casting, replica molding, micro inection molding, thermoplastic reshape en 3D print
* welke EM golven range hebben we (grafiek)
* explain reactive ion etching
* explain LIGA
* voor en nadelen van laser ablation
* richting van laserlicht perfect recht?

 
* wat is een excimer laser
* op welke basis werkt laser ablatie
* glass temperature melting temperature
* CFD
* navier stokes, hoe wordt chaotische advectie inrekening gebracht
* hoe wordt turbulentie gerekend
* solver methodes
* meshing methodes (central difference, ed)
<div class="notes">
  these are some notes to myself

  * note 1
  * note 2
</div>
---

# Chapter 3 {.fragile}

 
* Alternatieve technieken
  - T-junction
  - Flow focusing
    + EWOD (vorm van ElectroHydrolyc drive)
    + DEP - dielectrolysis
  - Review artikel Bubble formation and breakup dynamics in microfluidic devices by Taotao Fu and Younguang Ma
* wat is de partitie coefficient
* de afleiding
* wat is onmengbaar


 
* wat is fasescheiding
* hoe Kla afleiding gedaan
* lump sum factor
* verschil tussen enkele component en meerdere componenten
* correlatie U=IR en vloeistofverdeling
* afleiding drukval
* hagen-pousseuille drukval


 
* afleiding van Daniel Tondeur
* wat zit er allemaal in de lump sum factor $\alpha$
* wat is betekenis van Ca nummer
* berekening van STD en COV
* wat is creeping flow


 
* vergelijking voor fasescheiding
* effect sterische hinder fasescheiding
* squeezing, dripping en jetting regime
* hoe werkt flow focusing ifv flow ratio en flow rate
* wat zijn oplossingen voor vloeistof flow focusing in distributoren

---

# Chapter 4 {.fragile}
wat is een [beta-amino alcohol](http://www.organic-chemistry.org/synthesis/C1N/amines/betaaminoalcohols.shtm)
* enkele voorbeelden van extracties

* wat is de partitie coefficient

* de afleiding
* wat is onmengbaar
* wat is fasescheiding
* hoe Kla afleiding gedaan
* lump sum factor


 
* verschil tussen enkele component en meerdere componenten 
* wat is excitatie en fluorescentie (zie FITC)

* hoe werkt een half doorlaatfilter (dichroic filter)
* [bragg law](https://en.wikipedia.org/wiki/Bragg%27s_law)
* $2d sin \theta = (int) \lambda$

wat is een ccd (p-doped halfgeleider met spanningcel en uitlezing)

hoe werkt een lens

$pKa = -log_{10} \frac{H^+ A^-}{HA}$
dissociatie constante
des te lager pH des te sneller de dissociatie plaats vind voor pKa 


 
* pH, zuur/base
* fluorescein pH afhankelijkheid en golflengte
* Afleiding van Kla uit massabalans
* afleiding van Kla met energie dissipatie

 
* water in butanol
* water + fitc in butanol (succinic acid)
* fitc diffusie van water na butanol (met succinic acid)

---

# Chapter 5 {.fragile}

 
wat zijn andere manieren van fasescheiding buiten gravitair 
[info](http://www.enggcyclopedia.com/2011/05/liquid-liquid-separation/)

* op basis van affiniteit
  - membraan filtratie   
  - chromatografie
* op basis van verschil in densiteit
  - centrifuge
  - settler
  - hydrocycloon
* op basis van andere fysische eigenschappen
  - destillatie
  - magnetisch?


 
uitleggen over reactie en mass transfer limited reacties

* wat zijn de condities waaronder ze werken
* wat is laplace druk, hoe werkt een membraan
* cyanex 272
* verzeping
* pH curve voor extractie
* grafieken van E en Selectiviteit uitleggen


 
* wat is de partitie coefficient
* de afleiding
* wat is onmengbaar
* wat is fasescheiding
* waarom tegenstroom
* wat is kruisstroom, gelijkstroom, tegenstroom
* hoe Kla afleiding gedaan
* lump sum factor


 
* verschil tussen enkele component en meerdere componenten
* sterische hinder voor fasescheiding
* pH curve van cyanex 272 en Co/Ni extractie

---

# Chapter 6 {.fragile}

## Crystallization
* wat is een kristal structuur
* verschil amorf kristal
* waarom zeoliet en geen hydroxide ed
* groei vs nucleatie
* kinetiek


 
* samenstelling van grondstoffen voor NaA
* wat zijn zeolieten
* hoe werkt XRD
* wat is adsorptie, op welke basis werkt dit
* wat is absorptie, op welke basis
* doperen
* vergelijking voor warmte transfer/diffusie en afleiding
* Prandtl, Nusselt


 
> $Nu = \frac{hL}{k}$
> 
> $Q = \dot{m}c_p dT$
> 
> $U = \frac{k}/{\Delta x}$
> 
> $\frac{\Delta Q}{\Delta t} = U A(-\Delta T)$
> 
> laminair en constante wand T => Nu = 3.66
> 
> $T = Te-(Te-Ts)exp^{U(\pi r^2 L)/(\dot{m}c_p)}$
> 
>[developed flow](https://www.thermalfluidscentral.org/encyclopedia/index.php/Fully_Developed_Laminar_Flow_and_Temperature_Profile)

>[developing flow](http://www.thermalfluidscentral.org/encyclopedia/index.php/Thermally_developing_laminar_flow)


* wat is aan poisson fit, waarom deze fit
* hoe werkt laser scattering measurement

---

# Chapter 7 {.fragile}

 
* wat is algemene toekomst
* waar past de flow reactor toestand binnen Umicore
* Wat zijn voorwaarden waaronder ze zouden werken
* hoe verstopping voorkomen


---

# General {.fragile}

## diffusion
Nernst-Einstein equation for (diffusion at it's fullest)

$\frac{\delta c}{\delta t} + \nabla. (-D \nabla c_i + c_i u + z_i c_i u_i E) - R = 0$

with u the velocity and E the electrochemical flow if present

---

## navier-stokes
$\rho (\frac{\delta u}{\delta t} + u \nabla u) = - \nabla p + \nabla (\mu (\nabla u + (\nabla u)^{T}) - 2/3 \mu (\nabla u)I) + \F$

continuity equation for fluids with conservation of mass and momentum

$\frac{\delta \rho}{\delta t} + \nabla (\rho u) = 0$

valid for Kn < 0.01


##microwave heating
>Microwave heating is a multiphysics phenomenon that involves electromagnetic waves and heat transfer; any material that is exposed to electromagnetic radiation will be heated up. The rapidly varying electric and magnetic fields lead to four sources of heating. Any electric field applied to a conductive material will cause current to flow. In addition, a time-varying electric field will cause dipolar molecules, such as water, to oscillate back and forth. A time-varying magnetic field applied to a conductive material will also induce current flow. There can also be hysteresis losses in certain types of magnetic materials.


 
[microwave heating soup](https://www.comsol.com/multiphysics/microwave-heating)
>These phenomena will lead to current flow and oscillation of the molecules in the material. These losses will dissipate as heat and so cause a rise in temperature of the material. As the temperature of the material changes, the properties that govern how much heat is generated, such as the material's conductivity and the dielectric loss tangent, will change. In addition, the material-specific heat and thermal conductivity will also change. Microwave heating considers the coupling between all of these phenomena.

---

# Jury leden fields

## Tom Van Gerven
  - Proces intensificatie
    + US, UV, µ-wave, mechanical
  - liquid-fluid itneraction
  - water zuivering
  - reactor kennis

## Heidi Ottevaere
  - bphot
  - optica


## Tom Breugelmans
  - Electrochemie
  - oppervlakte chemie
  - katalyse
  - SX (Jonas)

## Johan Deconinck
  - Electrochemie
    + microcapillaire flow
  - CFD
  - SX

## Nico Vervoort
  - HPLC
  - CFD, berekeningen
  - Vloeistoverdelingen
  - PrepChrom

## Ken Broeckhoven
  - UHPLC
  - Kinetiek, mathematical mastermind
  - kinetic plot

## Kris Driesen
  - Hydrometallurgie
  - sol-gel chemie
  - catalyse
  - ionische vloeistoffen
  - luminescence
  - nanoparticles
  - electrochemie
  - spectroscopie

## Wim De Malsche 
  - HPLC
  - etsing
  - SX
  - optica
  - partitie coefficient